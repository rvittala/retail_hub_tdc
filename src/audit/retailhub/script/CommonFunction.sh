##MAILUNCTION ####
send_email() {
echo "####sending mail####"
(
echo To:"${MAIL_LIST}"
echo From:SVC-EDM@cat-production-vep6.localdomain
  echo "Content-Type: text/html; "
  echo Subject: Retail hub Spark Job Execution status as on `date --date="-1 days" '+%Y-%m-%d'`
  echo
 cat $Retail_Hub_SparkJob_mail_body
) | /usr/sbin/sendmail -t
echo "####email sent####"
}



mail_body()
{
Retailer_Name=$1
Retailer_Key=$2
Period_Type_Qty=$3
Target_Tabel_Name=$4
Load_Date=$5
Log=$6
Status=$7
Target_Tabel_Record_Count=$8
Start_Time=$9
End_Time=`date '+%Y-%m-%d %H:%M:%S'`

Retail_Hub_SparkJob_mail_body="/tmp/Retail_Hub_SparkJob_mail_body.html"
cat <<EOT >> ${Retail_Hub_SparkJob_mail_body}
<!DOCTYPE html>
<html>
<body text=\" \">
<p>
<pre><b>Hello All ,

 PFB STATUS FOR SPARK JOB EXECUTION STATUS :-</b>
 <b>Retailer_Name :</b>${Retailer_Name}
 <b>Retailer_Key  :</b> ${Retailer_Key}
 <b>Period_Type_Qty :</b>${Period_Type_Qty}
 <b>Target_Tabel_Name :</b>${Target_Tabel_Name}
 <b>Load_Date :</b> ${Load_Date}
 <b>Log :</b>${Log}
 <b>Status :</b>${Status}
 <b>Target_Tabel_Record_Count :</b> ${Target_Tabel_Record_Count}
 <b>Start_Time :</b>${Start_Time}
 <b>End_Time :</b>${End_Time}
 </pre>
 </p>
 </body>
 </HTML>
EOT
send_email
}
MAIL_LIST="Ashish.Agarwal@catalina.com"



#log print function
get_log_print()
{
 echo "$(date -u '+%Y-%m-%d') $1 "
 
}

#Mail
job_failing ()
{

mail_body $Retailer_Name $Retailer_Key $Period_Type_Qty $Tdc_Table $CURRENT_DATE $LOG "FAILED" 0 "$START_TIME"
rm /tmp/Retail_Hub_SparkJob_mail_body.html
exit 1
}


job_success ()
{

mail_body $Retailer_Name $Retailer_Key $Period_Type_Qty $Tdc_Table $CURRENT_DATE $LOG "SUCCESS" $TRGT_TABLE_RECORD_COUNT  "$START_TIME"
rm /tmp/Retail_Hub_SparkJob_mail_body.html
exit 1
}

