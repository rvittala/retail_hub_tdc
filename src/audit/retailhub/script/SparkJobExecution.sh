#SETTING USER SCRIPT,PROPERTY,JAR,LOG HOME PATH FOR RETAIL HUB SPARK JOB EXECUTION

SCRIPT_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
MODULE_HOME=`dirname ${SCRIPT_HOME}`
PROPERTY_HOME=${MODULE_HOME}/properties
JAR_HOME=${MODULE_HOME}/jar
source  ${SCRIPT_HOME}/CommonFunction.sh
source  $1
log_date=`date '+%Y-%m-%d_%H:%M:%S'`
LOG_HOME=${Log_Path}/${Retailer_Name}

if [ -d $LOG_HOME ]
   then
      get_log_print "[INFO :]log directory for RETAILER:${Retailer_Name} already exist"
   else
       get_log_print "[INFO :]Creating log directory for RETAILER:${Retailer_Name}"
      mkdir -p ${LOG_HOME}
fi

echo "${LOG_HOME}/RETAILER:${Retailer_Name}_SPARKJOB_EXECUTION_FOR_TABLE_${Tdc_Table}_${log_date}.log"
exec 1> ${LOG_HOME}/RETAILER:${Retailer_Name}_SPARKJOB_EXECUTION_FOR_TABLE_${Tdc_Table}_${log_date}.log 2>&1 


get_log_print "[INFO :]SCRIPT_HOME=${SCRIPT_HOME}"
get_log_print "[INFO :]MODULE_HOME=${MODULE_HOME}"
get_log_print "[INFO :]PROPERTY_HOME=${PROPERTY_HOME}"
get_log_print "[INFO :]LOG_HOME=${LOG_HOME}"
get_log_print "[INFO :]JAR_HOME=${JAR_HOME}"

#LOG PATH

LOG=${LOG_HOME}/RETAILER:${Retailer_Name}_SPARKJOB_EXECUTION_FOR_TABLE_${Tdc_Table}_${log_date}.log
get_log_print "[INFO :]LOG=${LOG}"

CURRENT_DATE=`date '+%Y-%m-%d'`
get_log_print "[INFO :]CURRENT_DATE=${CURRENT_DATE}"

START_TIME=`date '+%Y-%m-%d %H:%M:%S'`
get_log_print "[INFO :]START_TIME=${START_TIME}"



#Pre-Check if files exist or not 
if [ -f "${PROPERTY_HOME}/AuditMysqlCredential.properties" ]
    then
     get_log_print "[SUCCESS :]AuditMysqlCredential.properties file exits in path :"${PROPERTY_HOME}/""
    else
     get_log_print "[FAILURE :]]AuditMysqlCredential.properties file  does not exits in path :"${PROPERTY_HOME}/""
     job_failing
fi
     
 if [ -f "${PROPERTY_HOME}/BeelineCrendential.properties" ]
    then
      get_log_print "[SUCCESS :]BeelineCrendential.properties file exits in path :"${PROPERTY_HOME}/""
    else
      get_log_print "[FAILURE :]]BeelineCrendential.properties file  does not exits in path :"${PROPERTY_HOME}/""
    job_failing
  fi

 if [ -f "${PROPERTY_HOME}/${Spark_option_Code_Config_File}" ]
    then
     get_log_print "[SUCCESS :]${Spark_option_Code_Config_File} file exits in path :"${PROPERTY_HOME}/""
    else
     get_log_print "[FAILURE :]]${Spark_option_Code_Config_File} file  does not exits in path :"${PROPERTY_HOME}/""
     job_failing
  fi

 if [ -f "${JAR_HOME}/${Spark_Option_jar}" ]
    then
     get_log_print "[SUCCESS :]${Spark_Option_jar} file exits in path :"${JAR_HOME}/""
    else
     get_log_print "[FAILURE :]]${Spark_Option_jar} file  does not exits in path :"${JAR_HOME}/""
     job_failing
  fi



#EXTRACTING  AUDIT_MYSQL_CREDENTIAL

MYSQL_AUDITDB_IP=`grep -w "Ip" "${PROPERTY_HOME}/AuditMysqlCredential.properties" | cut -d '=' -f2 `
MYSQL_AUDITDB_USER=`grep -w "User" "${PROPERTY_HOME}/AuditMysqlCredential.properties" | cut -d '=' -f2`
MYSQL_AUDITDB_PASSWORD=`grep -w "Password" "${PROPERTY_HOME}/AuditMysqlCredential.properties" | cut -d '=' -f2 `

get_log_print "[INFO :]MYSQL_AUDITDB_IP=${MYSQL_AUDITDB_IP}"
get_log_print "[INFO :]MYSQL_AUDITDB_USER=${MYSQL_AUDITDB_USER}"
get_log_print "[INFO :]MYSQL_AUDITDB_PASSWORD=${MYSQL_AUDITDB_PASSWORD}" 

#EXTRACTING AUDIT_MYSQL_CONNECTION_URL
AUDIT_MYSQL_CONNECTION_URL="-h ${MYSQL_AUDITDB_IP} -u${MYSQL_AUDITDB_USER} -p${MYSQL_AUDITDB_PASSWORD}" 
get_log_print "[INFO :]AUDIT_MYSQL_CONNECTION_URL=${AUDIT_MYSQL_CONNECTION_URL}"

#EXTRACTING AUDIT_MYSQL_DATABASE_AND_TABLE
MYSQL_AUDITDB_DATABASE=${Mysql_Database}
MYSQL_AUDITDB_TABLE=${Mysql_Table}

get_log_print "[INFO :]Mysq_Database=${Mysql_Database}"
get_log_print "[INFO :]Mysql_Table=${Mysql_Table}"

#EXTRACTING  BEELINE CONNECTION URL
CEDL_BEELINE_HOST_NAME_PORT=`grep -w "CEDL_BEELINE_HOST_NAME_PORT" "${PROPERTY_HOME}/BeelineCrendential.properties" | cut -d '=' -f2 `
CEDL_BEELINE_USER=`grep -w "CEDL_BEELINE_USER" "${PROPERTY_HOME}/BeelineCrendential.properties" | cut -d '=' -f2 `
CEDL_BEELINE_PASSWORD=`grep -w "CEDL_BEELINE_PASSWORD" "${PROPERTY_HOME}/BeelineCrendential.properties" | cut -d '=' -f2 `

get_log_print "[INFO :]CEDL_BEELINE_HOST_NAME_PORT=${CEDL_BEELINE_HOST_NAME_PORT}"
get_log_print "[INFO :]CEDL_BEELINE_USER=${CEDL_BEELINE_USER}"
get_log_print "[INFO :]CEDL_BEELINE_PASSWORD=${CEDL_BEELINE_PASSWORD}"

#Extracting BEELINE CONNECTION URL

BEELINE_CONNECTION_URL="beeline -u 'jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2' -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"
get_log_print "[INFO :]BEELINE_CONNECTION_URL=${BEELINE_CONNECTION_URL}"


#EXTRACTNG TDC TABLE AND  DATABASE

TDC_DATABASE=${Tdc_Database}
TDC_TABLE=${Tdc_Table}

get_log_print "[INFO :]Tdc_Database=${Tdc_Database}"
get_log_print "[INFO :]Tdc_Table=${Tdc_Table}"

#EXTRACTING Retailer Information

RETAILER_NAME=$Retailer_Name
RETAILER_KEY=$Retailer_Key
PERIOD_TYPE_QTY=$Period_Type_Qty

get_log_print "[INFO :]RETAILER_NAME=${RETAILER_NAME}"
get_log_print "[INFO :]RETAILER_KEY=${RETAILER_KEY}"

#Extracting SPARK Config Options 
#SPARK SUBMIT OPTION

run_mode=$Spark_option_Run_Mode

if [ -z "$run_mode" ]
  then
     get_log_print "[INFO :] run_mode parameter value not found in argument "$1""
     job_failing
  else
     get_log_print "[INFO :] "run_mode="$run_mode"" value found in argument "$1""
  fi

class_name=$Spark_option_Class_Name

if [ -z "$class_name" ]
  then
     get_log_print "[INFO :] class_name parameter value not found in argument "$1""
     job_failing
  else
     get_log_print "[INFO :] "class_name="$class_name"" value found in argument "$1""
  fi


config_file=$Spark_option_Code_Config_File

if [ -z "$config_file" ]
  then
     get_log_print "[INFO :] category_config_file parameter value not found in argument "$1""
     job_failing
  else
     get_log_print "[INFO :] "category_config_file="$config_file"" value found in argument "$1""
  fi

driver_memory=$Spark_Option_driver_memory

if [ -z "$driver_memory" ]
  then
     get_log_print "[INFO :] driver_memory parameter value not found in argument "$1""
     job_failing
  else
     get_log_print "[INFO :] "driver_memory="$driver_memory"" value found in argument "$1""
  fi

num_executors=$Spark_Option_num_executors

if [ -z "$num_executors" ]
  then
     get_log_print "[INFO :] num_executors parameter value not found in argument "$1""
     job_failing
  else
     get_log_print "[INFO :] "num_executors="$num_executors"" value found in argument "$1""
  fi


executor_cores=$Spark_Option_executor_cores

if [ -z "$executor_cores" ]
  then
     get_log_print "[INFO :] executor_cores parameter value not found in argument "$1""
     job_failing
  else
     get_log_print "[INFO :] "executor_cores="$executor_cores"" value found in argument "$1""
  fi

deploy_mode=$Spark_Option_Deploy_mode

if [ -z "$deploy_mode" ]
  then
     get_log_print "[INFO :] deploy_mode parameter value not found in argument "$1""
     job_failing
  else
     get_log_print "[INFO :] "deploy_mode="$deploy_mode"" value found in argument "$1""
  fi

executor_memory=$Spark_Option_executor_memory

if [ -z "$executor_memory" ]
  then
     get_log_print "[INFO :] executor_memory parameter value not found in argument "$1""
     job_failing
  else
     get_log_print "[INFO :] "executor_memory="$executor_memory"" value found in argument "$1""
  fi

PERIOD_TYPE_QTY=`grep -w "Period_Type_Qty" $1 |  awk -F 'Period_Type_Qty=' '{print $2}' | tr -d '"'`
if [ -z "$PERIOD_TYPE_QTY" ]
  then
     get_log_print "[INFO :] PERIOD_TYPE_QTY parameter value not found in argument "$1""
     job_failing
  else
     get_log_print "[INFO :] "PERIOD_TYPE_QTY="$PERIOD_TYPE_QTY"" value found in argument "$1""
  fi


Dilimiter=$IFS
Static_conf=`grep -w "Spark_Option_conf_static" $1 |  awk -F 'Spark_Option_conf_static=' '{print $2}' | tr -d '"'`

                export IFS=","
                for word in $Static_conf; do
                 spark_conf_static+="--conf $word "
                done
IFS=$Dilimiter

if [ -z "$spark_conf_static" ]
  then
     get_log_print "[INFO :] spark_conf_static parameter value not found in argument "$1""
     job_failing
  else
     get_log_print "[INFO :] "spark_conf_static="$spark_conf_static"" value found in argument "$1""
  fi


dyn_conf=`grep -w "Spark_Option_conf_dynamic" $1 |  awk -F 'Spark_Option_conf_dynamic=' '{print $2}' | tr -d '"'`
                export IFS=","
                for word in $dyn_conf; do
                 spark_conf_dyn+="--conf $word "
                done

IFS=$Dilimiter
			
if [ -z "$dyn_conf" ]
  then
     get_log_print "[INFO :] dyn_conf parameter value not found in argument "$1""
     job_failing
  else
     get_log_print "[INFO :] "spark_conf_dyn="$spark_conf_dyn"" value found in argument "$1""
  fi



 jar=$Spark_Option_jar


if [ -z "$jar" ]
  then
     get_log_print "[INFO :] jar parameter value not found in argument "$1""
     job_failing
  else
     get_log_print "[INFO :] "jar="$jar"" value found in argument "$1""
  fi



#get max load and status from audit table 

LOAD_DATE=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select max(load_date) from $MYSQL_AUDITDB_DATABASE.$MYSQL_AUDITDB_TABLE where Retailer_key=$RETAILER_KEY AND Target_Table_Name='${Tdc_Table}'"`
   if [ $? -eq 0 ]
      then 
      get_log_print "[SUCCESS :]LOAD_DATE=${LOAD_DATE}"
      else
      get_log_print "[FAILURE :]FAILED TO FETCH LOAD_DATE FROM MYSQL AUDIT TABLE=$MYSQL_AUDITDB_DATABASE.$MYSQL_AUDITDB_TABLE"
      job_failing     
   fi

JOB_STATUS=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select Status from $MYSQL_AUDITDB_DATABASE.$MYSQL_AUDITDB_TABLE where Retailer_key=$RETAILER_KEY AND Target_Table_Name='${Tdc_Table}' AND load_date='${LOAD_DATE}'"`
    if [ $? -eq 0 ]
      then
      get_log_print "[SUCCESS :]JOB_STATUS=${JOB_STATUS}"
      else
      get_log_print "[FAILURE :]FAILED TO FETCH JOB_STATUS FROM MYSQL AUDIT TABLE=$MYSQL_AUDITDB_DATABASE.$MYSQL_AUDITDB_TABLE"
      job_failing 
   fi

AUDIT_ID=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select Audit_Id from $MYSQL_AUDITDB_DATABASE.$MYSQL_AUDITDB_TABLE order by Start_Time desc limit 1"`
if [ $? -eq 0 ]
      then
      get_log_print "[SUCCESS :]AUDIT_ID=${AUDIT_ID}"
      else
      get_log_print "[FAILURE :]FAILED TO FETCH AUDIT_ID FROM MYSQL AUDIT TABLE=$MYSQL_AUDITDB_DATABASE.$MYSQL_AUDITDB_TABLE"
      job_failing
 fi



if [ "$CURRENT_DATE" = "$LOAD_DATE" ]
   then  
       get_log_print "[INFO:]SPARK JOB ALREADY RAN TODAY FOR RETAILER:${RETAILER_NAME} AND TABLE:${Tdc_Table}. SPARK JOB WILL RERUN  ONLY IF JOB_STATUS IS FAILED "
        if [ "$JOB_STATUS" = "RUNNING" ]
	       then
	       get_log_print "[INFO:]SPARK JOB LAST EXECUTION IS ON RUNNING STATE FOR RETAILER:${RETAILER_NAME} AND TABLE:${Tdc_Table}. RERUN IS NOT ALLOWED IF JOB_STATUS=${JOB_STATUS}"
               job_failing
           elif [ "$JOB_STATUS" = "FAILED" ]
                then 
                get_log_print "[INFO:]SPARK JOB LAST EXECUTION FAILED FOR RETAILER : ${RETAILER_NAME} AND TABLE:${Tdc_Table} SO RERUNNING SPARK JOB ONCE AGAIN"
                mysql ${AUDIT_MYSQL_CONNECTION_URL} -sN -e "UPDATE ${MYSQL_AUDITDB_DATABASE}.${MYSQL_AUDITDB_TABLE}  SET Period_Type_Qty='${PERIOD_TYPE_QTY}',Log='${LOG}',Status='RUNNING',Target_Table_Record_Count=0,Start_Time='$START_TIME',End_Time='NA' where load_date='$LOAD_DATE'AND Target_Table_Name='$TDC_TABLE' AND Retailer_Name='$RETAILER_NAME'"
                if [ $? -eq  0 ]
			       then
				   get_log_print "[SUCCESS:]MYSQL UPDATE SUCESSFULLY EXECUTED"
				   else 
				   get_log_print "[FAILED:]FAILED TO UPDATE MYSQL"
				   job_failing
			    fi
	    else 
	      get_log_print "[INFO:]Spark JOB LAST EXECUTION ALREADY COMPLETED TODAY  ${RETAILER_NAME} AND TABLE:${Tdc_Table}.RERUN ON SAME DAY IS NOT ALLOWED if JOB_STATUS=SUCCESS"
              job_failing
	    fi
else  
        if [ "$JOB_STATUS" = "RUNNING" ]
               then
                 get_log_print "SPARK JOB LAST EXECUTION IS ON RUNNING STATE FOR RETAILER : ${RETAILER_NAME}.RERUN IS NOT ALLOWED IF JOB_STATUS=${JOB_STATUS} "
                 job_failing
        else
             Audit_Nbr=`echo $AUDIT_ID | awk -F_ '{print $NF}'`
                   if [ -z $Audit_Nbr ]
                      then
                         Audit_Id=${Retailer_Name}_${Retailer_Key}_${Tdc_Table}_1
                      else 
                         Audit_Id_Nbr=`expr $Audit_Nbr + 1`
 
                         Audit_Id=${Retailer_Name}_${Retailer_Key}_${Tdc_Table}_${Audit_Id_Nbr}
                   fi

             mysql ${AUDIT_MYSQL_CONNECTION_URL} -sN -e "Insert into ${MYSQL_AUDITDB_DATABASE}.${MYSQL_AUDITDB_TABLE}  values('${Audit_Id}','${RETAILER_NAME}',${RETAILER_KEY},'${PERIOD_TYPE_QTY}','${TDC_TABLE}','${CURRENT_DATE}','${LOG}','RUNNING',0,'${START_TIME}','NA')"
                     if [ $? -eq  0 ]
			then
                        get_log_print "[SUCCESS:]MYSQL INSERT SUCESSFULLY EXECUTED"
                        else 
                        get_log_print "[FAILED:]FAILED TO INSERT MYSQL"
                        job_failing
			fi
					 
				
	 fi
fi


#Running SPARK JOB
  

  if [[ "$run_mode" = "static" ]]
      then
          get_log_print "INFO : SPARK SUBMIT JOB STARTED"
          spark-submit --class ${class_name} --master yarn-${deploy_mode} --deploy-mode ${deploy_mode} --driver-memory ${driver_memory} --num-executors ${num_executors} --executor-cores ${executor_cores} --executor-memory ${executor_memory} ${spark_conf_static}  --files ${PROPERTY_HOME}/${config_file} ${JAR_HOME}/${jar} \""${PERIOD_TYPE_QTY}"\" "2019-06-16" "2019-06-22" ${RETAILER_KEY} ${CURRENT_DATE}

              if [ $? -eq 0 ]
                 then
                   get_log_print "[SUCCESS ]:SPARK SUBMIT JOB SUCESSFULLY COMPLETED"
                   
		   mysql ${AUDIT_MYSQL_CONNECTION_URL} -sN -e "UPDATE ${MYSQL_AUDITDB_DATABASE}.${MYSQL_AUDITDB_TABLE}  SET Status='SUCCESS'  where load_date='${CURRENT_DATE}' AND Target_Table_Name='$TDC_TABLE' AND Retailer_Name='$RETAILER_NAME'"
                     
                      if [ $? -eq  0 ]
                       then
                          get_log_print "[SUCCESS:]MYSQL UPDATE SUCESSFULLY EXECUTED"
                       else
                          get_log_print "[FAILED:]FAILED TO UPDATE MYSQL "
                          job_failing
                    fi
              else
                     get_log_print "[ERROR:]SPARK SUBMIT JOB IS FAILED"
                     End_Time=$(date '+%Y-%m-%d %H:%M:%S')
                     mysql ${AUDIT_MYSQL_CONNECTION_URL} -sN -e "UPDATE ${MYSQL_AUDITDB_DATABASE}.${MYSQL_AUDITDB_TABLE}  SET Status='FAILED',End_Time='$End_Time'  where load_date='${CURRENT_DATE}'AND Target_Table_Name='$TDC_TABLE' AND Retailer_Name='$RETAILER_NAME'"
                     if [ $? -eq  0 ]
                        then
                           get_log_print "[SUCCESS:]MYSQL UPDATE SUCESSFULLY EXECUTED"
                        else
                           get_log_print "[FAILED:]FAILED TO UPDATE MYSQL "
                           job_failing
                     fi
                       job_failing
               fi
 else
      get_log_print "INFO : SPARK SUBMIT JOB STARTED"
       spark-submit --class ${class_name} --master yarn-${deploy_mode} --deploy-mode ${deploy_mode}  --driver-memory ${driver_memory}  --executor-cores ${executor_cores}  --executor-memory ${executor_memory} ${spark_conf_dyn} --files ${PROPERTY_HOME}/${config_file} ${JAR_HOME}/${jar} \""${PERIOD_TYPE_QTY}"\" "2019-06-16" "2019-06-22" ${RETAILER_KEY} ${CURRENT_DATE}

         if [ $? -eq 0 ]
         then
             get_log_print "[Success :]SPARK SUBMIT JOB SUCESSFULLY COMPLETED"
              mysql ${AUDIT_MYSQL_CONNECTION_URL} -sN -e "UPDATE ${MYSQL_AUDITDB_DATABASE}.${MYSQL_AUDITDB_TABLE}  SET Status='SUCCESS'  where load_date='${CURRENT_DATE}' AND Target_Table_Name='$TDC_TABLE' AND Retailer_Name='$RETAILER_NAME'"

                      if [ $? -eq  0 ]
                       then
                          get_log_print "[SUCCESS:]MYSQL UPDATE SUCESSFULLY EXECUTED"
                       else
                          get_log_print "[FAILED:]FAILED TO UPDATE MYSQL "
                          job_failing
                    fi

         else
             get_log_print "[ERROR:]SPARK SUBMIT JOB IS FAILED"
             End_Time=$(date '+%Y-%m-%d %H:%M:%S')
             mysql ${AUDIT_MYSQL_CONNECTION_URL} -sN -e "UPDATE ${MYSQL_AUDITDB_DATABASE}.${MYSQL_AUDITDB_TABLE}  SET Status='FAILED',End_Time='$End_Time'  where load_date='${CURRENT_DATE}'AND Target_Table_Name='$TDC_TABLE' AND Retailer_Name='$RETAILER_NAME'"
                     if [ $? -eq  0 ]
                        then
                           get_log_print "[SUCCESS:]MYSQL UPDATE SUCESSFULLY EXECUTED"
                        else
                           get_log_print "[FAILED:]FAILED TO UPDATE MYSQL "
                           job_failing
                     fi
             job_failing

         fi
 fi


        
#POST-VALIDATION

TRGT_TABLE_RECORD_COUNT=`${BEELINE_CONNECTION_URL} --showHeader=false --outputformat=csv2  -e "select count(*) cnt from ${Tdc_Database}.${Tdc_Table} where parent_corporation_key=${Retailer_Key}  AND  load_date=\"${CURRENT_DATE}\" limit 1 "`

 if  [ $? -eq 0 ]
  then
     get_log_print "[INFO :]TRGT_TABLE_RECORD_COUNT=${TRGT_TABLE_RECORD_COUNT}"
      mysql ${AUDIT_MYSQL_CONNECTION_URL} -sN -e "UPDATE ${MYSQL_AUDITDB_DATABASE}.${MYSQL_AUDITDB_TABLE}  SET Target_Table_Record_Count=$TRGT_TABLE_RECORD_COUNT  where load_date='${CURRENT_DATE}' AND Target_Table_Name='$TDC_TABLE' AND Retailer_Name='$RETAILER_NAME'"
                      if [ $? -eq  0 ]
                       then
                          get_log_print "[SUCCESS:]MYSQL UPDATE SUCESSFULLY EXECUTED"
                       else
                          get_log_print "[FAILED:]FAILED TO UPDATE MYSQL "
                          job_failing
                    fi
  else
     get_log_print "[FAILURE :]ERROR WHILE FETCHING TRGT_TABLE_RECORD_COUNT"
     job_failing
 fi

#Testing landing directory exists or not for table

#Deleting Old Partiton data from  Final TDC Hive Table And keep the full 
MIN_LOAD_DATE=`${BEELINE_CONNECTION_URL} --showHeader=false --outputformat=csv2  -e "select distinct load_date  from ${Tdc_Database}.${Tdc_Table} where parent_corporation_key=${Retailer_Key} AND load_date<'${CURRENT_DATE}'"`
  

        if [ -z "$MIN_LOAD_DATE" ]
           then
               get_log_print "[SUCCESS:]MYSQL MIN_LOAD_DATE value is Null.Since it has only one  partition hence not deleting it"
               End_Time=$(date '+%Y-%m-%d %H:%M:%S')
               mysql ${AUDIT_MYSQL_CONNECTION_URL} -sN -e "UPDATE ${MYSQL_AUDITDB_DATABASE}.${MYSQL_AUDITDB_TABLE}  SET End_Time='${End_Time}'  where load_date='${CURRENT_DATE}' AND Target_Table_Name='$TDC_TABLE' AND Retailer_Name='$RETAILER_NAME'"
               if [ $? -eq  0 ]
                  then
                     get_log_print "[SUCCESS:]MYSQL UPDATE SUCESSFULLY EXECUTED"
                   else
                     get_log_print "[FAILED:]FAILED TO UPDATE MYSQL "
                     job_failing
               fi
                     job_success
            else
                get_log_print "[SUCCESS:]MYSQL MIN_LOAD_DATE=$MIN_LOAD_DATE"
                hadoop fs -test -d "${Table_Data_Path}/${Tdc_Table}/parent_corporation_key=${Retailer_Key}/${Partiton_Column}=${MIN_LOAD_DATE}"
                if [ $? -eq 0 ]
                     then
                         get_log_print "[SUCCESS :]start Deleting directory ${Table_Data_Path}/${Tdc_Table}/parent_corporation_key=${Retailer_Key}/${Partiton_Column}=${MIN_LOAD_DATE}"
                         echo "hadoop fs -rm -r ${Table_Data_Path}/${Tdc_Table}/parent_corporation_key=${Retailer_Key}/${Partiton_Column}=${MIN_LOAD_DATE}"
                        hadoop fs -rm -r "${Table_Data_Path}/${Tdc_Table}/parent_corporation_key=${Retailer_Key}/${Partiton_Column}=${MIN_LOAD_DATE}"
                         if [ $? -eq 0 ]
                             then
                                get_log_print "[INFO :]Deleted directory ${Table_Data_Path}/${Tdc_Table}/parent_corporation_key=${Retailer_Key}/${Partiton_Column}=${MIN_LOAD_DATE}"
                               ${BEELINE_CONNECTION_URL} -e "Alter table ${Tdc_Database}.${Tdc_Table} DROP  if exists partition(parent_corporation_key=${Retailer_Key},${Partiton_Column}='${MIN_LOAD_DATE}')"
                                 if [ $? -eq 0 ]
                                  then
                                      get_log_print "[INFO :]Alter table completed "
                                  else
                                      get_log_print "[INFO :]Issue while Altering table "
                                      job_failing
                                 fi
                             else
                                 get_log_print "[INFO :]Error while deleting directory ${Table_Data_Path}/${Tdc_Table}/parent_corporation_key=${Retailer_Key}/${Partiton_Column}=${MIN_LOAD_DATE}"
                                 job_failing
                         fi

                fi

    fi



