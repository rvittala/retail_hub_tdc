package com.catalina.retailhub.utils

import java.util.Properties

import org.apache.spark.sql.{DataFrame, SparkSession}
import RetailHubException._

trait CommonUtils {
  def createSparkSession(className: String): SparkSession = {
    val spark = SparkSession.
      builder().
      appName(className).
      enableHiveSupport().
      config("hive.exec.dynamic.partition", "true").
      config("hive.exec.dynamic.partition.mode", "nonstrict").
      config("spark.sql.parquet.writeLegacyFormat", "true").
      config("hive.exec.max.dynamic.partitions", 1500).
      config("spark.sql.crossJoin.enabled", "true").
      config("spark.sql.files.maxPartitionBytes",255219000)
    .getOrCreate()
    spark
  }



def checkAndSetProperty(prop:Properties,propertyName:String):String = {
  if(!prop.containsKey(propertyName))
    throw new MissingPropertyException("Property \""+propertyName+"\" is missing.Set the value of property \""+propertyName+"\" properly" )
  else
  if(prop.getProperty(propertyName).isEmpty)
  throw new InvalidPropertyException("Property \""+propertyName+"\" is empty.Set the value of property \""+propertyName+"\" properly" )
  else
    prop.getProperty(propertyName)
}






}
