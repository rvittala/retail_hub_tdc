package com.catalina.retailhub.utils

object RetailHubException {
  case class InvalidArgsException(message:String) extends Exception(message)
  case class InvalidPropertyException(message:String) extends Exception(message)
  case class MissingPropertyException(message:String) extends Exception(message)
}
