package com.catalina.retailhub.reports

import com.catalina.retailhub.utils.CommonUtils
import java.io.FileInputStream
import java.util.Properties

import com.catalina.retailhub.reports.CategoryMetrics.createSparkSession
import com.catalina.retailhub.utils.CommonUtils
import com.catalina.retailhub.utils.RetailHubException.InvalidPropertyException
import org.apache.spark.api.java.StorageLevels
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions._

object CreateTable extends CommonUtils {
  val logger = org.apache.log4j.LogManager.getLogger(this.getClass)
  logger.info("Starting Aggregation Logic For Category Reporting")
  var ordersTblName: String = _
  var tdcRefinedSchemaName: String = _
  var cacheStrtDte: String = _
  var cacheEndDte: String = _
  var retailerKey:Int=_
  var parquetOrdersTblName: String = _
  var parquetSchemaName: String = _
  def main(args: Array[String]): Unit = {
    val spark = createSparkSession(this.getClass.getName)
    import spark.implicits._
    retailerKey = args(0).replaceAll("\\s", "").toInt
    cacheStrtDte = args(1).replaceAll("\\s", "")
    cacheEndDte = args(2).replaceAll("\\s", "")
    val propertyFile = args(3).replaceAll("\\s", "")
    parquetOrdersTblName = args(5).replaceAll("\\s", "")
    parquetSchemaName = args(4).replaceAll("\\s", "")

      val prop = new Properties();
      prop.load(new FileInputStream(propertyFile))
      ordersTblName = checkAndSetProperty(prop, "ordersTblName")
      tdcRefinedSchemaName = checkAndSetProperty(prop, "tdcRefinedSchemaName")


    val ordTblDf =(StartDate: String, EndDate: String) => spark.sql(s"SELECT id,retailer.parent_corporation_name,retailer.parent_corporation_key,retailer.chain_id," +
      s"retailer.store_id,date,transactions,load_date FROM $tdcRefinedSchemaName.$ordersTblName WHERE country_code='USA' and " +
      s"load_date >= '$StartDate' and load_date <= '$EndDate' AND retailer.parent_corporation_key='$retailerKey'")
      .select($"id", $"chain_id", $"store_id", $"parent_corporation_key", $"date", explode($"transactions").alias("trans"), $"load_date")
      .select($"id", $"chain_id", $"store_id", $"parent_corporation_key".as("parent_corp_key"), $"date", $"load_date",$"trans.upc".as("trans_upc"),
        $"trans.amount".as("amount"), $"trans.quantity".as("quantity"))
    val ordDf = ordTblDf(s"${cacheStrtDte}",s"${cacheEndDte}")

    ordDf
      .write
      .mode(SaveMode.Append)
      .format("parquet")
      .partitionBy("load_date").saveAsTable(s"${parquetSchemaName}.${parquetOrdersTblName}")
  }

}