package com.catalina.retailhub.reports

import java.io.FileInputStream
import java.util.Properties

import com.catalina.retailhub.utils.CommonUtils
import com.catalina.retailhub.utils.RetailHubException.InvalidPropertyException
import org.apache.spark.api.java.StorageLevels
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions._

object CategoryMetricAggsDev extends CommonUtils {
  val logger = org.apache.log4j.LogManager.getLogger(this.getClass)
  logger.info("Starting Aggregation Logic For Category Reporting")
  var ordersTblName:String = _
  var tdcRefinedSchemaName:String =_
  var retailerSchemaName:String = _
  var retailerTblName:String = _
  var productHierarchySchema:String = _
  var productHierarchyTblName:String = _
  var ownBrandListSchemaName:String = _
  var ownBrandListTblName:String = _
  var targetSchema:String = _
  var targetTblName:String = _
  var productLevelDimSchemaName:String = _
  var productLevelDimTblName:String = _
  var periodLkuDimSchema:String = _
  var periodLkuTblName:String=_
  var periodTypQtyString:String =_
  var startDate:String =_
  var endDate:String =_
  var retailerKey:Int=_
  var loadDte:String=_
  var owner:String=_

  case class df(ordDf:(String,String)=>DataFrame,prdHrchyDf:DataFrame,productLevelLkpUpDf:DataFrame,ownBrandDf:DataFrame)
  case class DateRanges(currWkStrtDte:String,currWkEndDte:String,rollWkStrtDte:String,rollWkEndDte:String,ygoWkStrtDte:String,ygoWkEndDte:String,periodTypQty:String,periodSk:String)

  def main(args: Array[String]): Unit = {
    val spark = createSparkSession(this.getClass.getName)
    logger.info("STARTING DATA AGGREGATION FOR CATEGORY REPORTING")
    spark.conf.set("spark.sql.files.maxPartitionBytes",255219000)
    periodTypQtyString = args(0).replaceAll("\\s", "")
    startDate = args(1).replaceAll("\\s", "")
    endDate = args(2).replaceAll("\\s", "")
    retailerKey = args(3).replaceAll("\\s", "").toInt
    val propertyFile = args(4).replaceAll("\\s", "")
    loadDte = args(5).replaceAll("\\s", "")

    try {
      val prop = new Properties();
      prop.load(new FileInputStream(propertyFile))
      ordersTblName = checkAndSetProperty(prop, "ordersTblName")
      tdcRefinedSchemaName = checkAndSetProperty(prop,"tdcRefinedSchemaName")
      retailerSchemaName = checkAndSetProperty(prop,"retailerSchemaName")
      retailerTblName = checkAndSetProperty(prop,"retailerTblName")
      productHierarchySchema = checkAndSetProperty(prop,"productHierarchySchema")
      productHierarchyTblName = checkAndSetProperty(prop,"productHierarchyTblName")
      ownBrandListSchemaName = checkAndSetProperty(prop,"ownBrandListSchemaName")
      ownBrandListTblName = checkAndSetProperty(prop,"ownBrandListTblName")
      targetSchema = checkAndSetProperty(prop,"targetSchema")
      targetTblName = checkAndSetProperty(prop,"targetTblName")
      productLevelDimSchemaName = checkAndSetProperty(prop,"productLevelDimSchemaName")
      productLevelDimTblName = checkAndSetProperty(prop,"productLevelDimTblName")
      periodLkuDimSchema = checkAndSetProperty(prop,"periodLkuDimSchema")
      periodLkuTblName = checkAndSetProperty(prop,"periodLkuTblName")
      owner = checkAndSetProperty(prop,"owner")
    }
    catch {
      case e:Exception =>logger.error(e)
        System.exit(100)

    }
    logger.info("periodTypQtyString="+periodTypQtyString)
    logger.info("startDate="+startDate)
    logger.info("endDate="+endDate)
    logger.info("retailerKey="+retailerKey)
    logger.info("propertyFile="+propertyFile)
    logger.info("loadDte="+loadDte)
    logger.info("ordersTblName="+ordersTblName)
    logger.info("owner="+owner)


    import spark.implicits._
    /* Orders table data for the period starting at "startDate" and ending at "endDate"*/
    val ordTblDf =(StartDate: String, EndDate: String) => spark.sql(s"SELECT id,retailer.parent_corporation_name,retailer.parent_corporation_key,retailer.chain_id," +
      s"retailer.store_id,date,transactions,load_date FROM $tdcRefinedSchemaName.$ordersTblName WHERE country_code='USA' and " +
      s"load_date >= '$StartDate' and load_date <= '$EndDate' AND retailer.parent_corporation_key='$retailerKey'")
      .select($"id", $"chain_id", $"store_id", $"parent_corporation_key", $"date", explode($"transactions").alias("trans"), $"load_date")
      .select($"id", $"chain_id", $"store_id", $"parent_corporation_key".as("parent_corp_key"), $"date", $"load_date",$"trans.upc".as("trans_upc"),
        $"trans.amount".as("amount"), $"trans.quantity".as("quantity"))

    /*GET THE PRODUCT HIERARCHY LEVEL*/
    val prdHrchyDf = spark.sql(s"SELECT trade_item_cd as upc_code," +
      s"trade_item_hier_l1_cd as product_level_1_code,trade_item_hier_l1_desc as product_hierarchy_level_1_description," +
      s"trade_item_hier_l2_cd as product_hierarchy_level_2_code,trade_item_hier_l2_desc as product_hierarchy_level_2_description," +
      s"trade_item_hier_l3_cd as product_hierarchy_level_3_code,trade_item_hier_l3_desc as product_hierarchy_level_3_description," +
      s"trade_item_hier_l4_cd as product_hierarchy_level_4_code,trade_item_hier_l4_desc as product_hierarchy_level_4_description," +
      s"trade_item_hier_l5_cd as product_hierarchy_level_5_code,trade_item_hier_l5_desc as product_hierarchy_level_5_description" +
      s" FROM $productHierarchySchema.$productHierarchyTblName where owner='$owner'").persist(StorageLevels.MEMORY_AND_DISK)

    /*GET THE SEQUENTIAL PRODUCT LEVELS*/
    val productLevelLkpUpDf = spark.sql(s"select parent_corp_key,sequential_product_level,product_level,description from $productLevelDimSchemaName.$productLevelDimTblName " +
      s"where parent_corp_key=$retailerKey").persist(StorageLevels.MEMORY_AND_DISK)

    /*GET ALL THE DATE RANGES FOR WHICH AGGREGATES NEEDS TO BE POPULATED*/
    val periodLkuDf = spark.sql(s"SELECT period_sk,period_typ_qty,period_begin_dt,period_end_dt FROM $periodLkuDimSchema.$periodLkuTblName " +
      s"WHERE period_begin_dt>= '$startDate' AND period_end_dt<='$endDate'").persist(StorageLevels.MEMORY_AND_DISK)

    /*GET THE OWN BRAND LIST FOR THE RETAILER */
    val ownBrandDf = spark.sql(s"select upc_cd as upc from $ownBrandListSchemaName.$ownBrandListTblName where owner='$owner'").persist(StorageLevels.MEMORY_AND_DISK)

    val dataFrames = df(ordTblDf,prdHrchyDf,productLevelLkpUpDf,ownBrandDf)
    val periodTypeQtyList = periodTypQtyString.split(",").toList

    /*ITERATE OVER THE periodTypeQtyList VALUES TO POPULATE AGGREGATES*/
    for (periodTypQty <- periodTypeQtyList) {
      var periodDateRangesList = periodLkuDf.filter(col("period_typ_qty") === periodTypQty).select($"period_begin_dt",$"period_end_dt",$"period_sk").
        as[(String,String,String)].collect().toList.reverse
      for (wkDateRange <- periodDateRangesList) {
        createCategoryReportingAggregates(spark,wkDateRange._1,wkDateRange._2,dataFrames,periodTypQty,wkDateRange._3)
      }
    }

    prdHrchyDf.unpersist()
    productLevelLkpUpDf.unpersist()
    periodLkuDf.unpersist()
    ownBrandDf.unpersist()

  }


  /*FUNCTION TO POPULATE CATEGORY REPORTING AGGREGATES*/
  def createCategoryReportingAggregates(spark:SparkSession,wkStrtDte:String,wkEndDte:String,dataFrames:df,periodTypQty:String,periodSk:String)={
    import  spark.implicits._
    val weekNo = periodTypQty.toInt
    val dateDf = spark.sql(s"select date_sub('${wkStrtDte}',${weekNo} * 7) as rollStrt,date_sub('${wkEndDte}',${weekNo} * 7) as rollEnd," +
      s"date_sub('${wkStrtDte}',52 * 7) as ygoStrt ,date_sub('${wkEndDte}',52 * 7) as ygoEnd").select($"rollStrt",$"rollEnd",$"ygoStrt",$"ygoEnd").persist(StorageLevels.MEMORY_ONLY_SER)

    val(rollWkStrtDte,rollWkEndDte,ygoWkStrtDte,ygoWkEndDte) = dateDf.as[(String,String,String,String)].collect()(0)
    logger.info("rollWkStrtDte="+rollWkStrtDte)
    logger.info("rollWkEndDte="+rollWkEndDte)
    logger.info("ygoWkStrtDte="+ygoWkStrtDte)
    logger.info("ygoWkEndDte="+ygoWkEndDte)
    val dateRanges= DateRanges(wkStrtDte,wkEndDte,rollWkStrtDte,rollWkEndDte,ygoWkStrtDte,ygoWkEndDte,periodTypQty,periodSk)
    val currDf=dataFrames.ordDf(s"${dateRanges.currWkStrtDte}",s"${dateRanges.currWkEndDte}").persist(StorageLevels.MEMORY_AND_DISK_SER)
    currDf.count()
    val rollDf = dataFrames.ordDf(s"${dateRanges.rollWkStrtDte}}",s"${dateRanges.rollWkEndDte}").persist(StorageLevels.MEMORY_AND_DISK_SER)
    rollDf.count()
    val yagoDf = if(!dateRanges.periodTypQty.equals("52")) dataFrames.ordDf(s"${dateRanges.ygoWkStrtDte}",s"${dateRanges.ygoWkEndDte}").persist(StorageLevels.MEMORY_AND_DISK_SER)
    else {spark.emptyDataFrame}
    yagoDf.count()
    val departmentAggregatesDf = processData(spark,currDf,rollDf,yagoDf,dateRanges,dataFrames,"Department","")

    departmentAggregatesDf
      .write
      .mode(SaveMode.Append)
      .format("orc")
      .partitionBy("parent_corp_key","load_date").saveAsTable(s"${targetSchema}.${targetTblName}")
    val categoryAggregatesDf = processData(spark,currDf,rollDf,yagoDf,dateRanges,dataFrames,"Category", "Department")
    categoryAggregatesDf
      .write
      .mode(SaveMode.Append)
      .format("orc")
      .partitionBy("parent_corp_key","load_date")
      .saveAsTable(s"${targetSchema}.${targetTblName}")
    val subcategoryAggregatesDf = processData(spark,currDf,rollDf,yagoDf,dateRanges,dataFrames,"Subcategory", "Category")
    subcategoryAggregatesDf
      .write
      .mode(SaveMode.Append)
      .format("orc")
      .partitionBy("parent_corp_key","load_date")
      .saveAsTable(s"${targetSchema}.${targetTblName}")

    dateDf.unpersist()
    currDf.unpersist()
    rollDf.unpersist()
    yagoDf.unpersist()
  }


  /*FUNCTION TO POPULATE AGGREGATES FOR SUBCATEGORY,CATEGORY,DEPARTMENT AND OTHER PRODUCT LEVELS */
  def processData(spark: SparkSession,currOrdTblDf:DataFrame,rollingOrdTblDf:DataFrame,yagoOrdTblDf:DataFrame,dateRanges:DateRanges,dataFrames:df,productLevel:String,parentProductLevel:String): DataFrame = {
    import spark.implicits._

    /*GET THE ACTIVE STORES ORDERS*/
    val (currOrdActiveStoresWithRollingDf, rollingOrdActiveStoresDf) = getOrdWithActiveStores(spark, currOrdTblDf, rollingOrdTblDf,dateRanges,"rolling",dataFrames.ownBrandDf)
    val (currOrdActiveStoresWithYagoDf, yagoOrdActiveStoresDf) = if(!dateRanges.periodTypQty.equals("52"))
      getOrdWithActiveStores(spark, currOrdTblDf, yagoOrdTblDf,dateRanges,"yago",dataFrames.ownBrandDf) else (spark.emptyDataFrame, spark.emptyDataFrame)


    /*GET PRODUCT HIERARCHY LEVEL*/
    val currRollingDf = getProductHierarchy(spark, currOrdActiveStoresWithRollingDf, dataFrames.prdHrchyDf, dataFrames.productLevelLkpUpDf,productLevel,parentProductLevel)
    val rollingDf = getProductHierarchy(spark, rollingOrdActiveStoresDf, dataFrames.prdHrchyDf, dataFrames.productLevelLkpUpDf,productLevel, parentProductLevel)
    val currYagoDf = if(!dateRanges.periodTypQty.equals("52"))
      getProductHierarchy(spark, currOrdActiveStoresWithYagoDf, dataFrames.prdHrchyDf, dataFrames.productLevelLkpUpDf, productLevel, parentProductLevel)
    else
      spark.emptyDataFrame

    val YagoDf = if(!dateRanges.periodTypQty.equals("52"))
      getProductHierarchy(spark, yagoOrdActiveStoresDf, dataFrames.prdHrchyDf, dataFrames.productLevelLkpUpDf, productLevel,parentProductLevel)
    else
      spark.emptyDataFrame


    /*POPULATE THE AGGREGATES FOR CURRENT , ROLLING AND YAGO PERIOD*/
    val currRollingAggDf = createAgg(spark, currRollingDf)
    val rollingAggDf = createAgg(spark, rollingDf)
    val currYagoAggDf = if(!dateRanges.periodTypQty.equals("52")) createAgg(spark, currYagoDf) else spark.emptyDataFrame
    val yagoAggDf = if(!dateRanges.periodTypQty.equals("52")) createAgg(spark, YagoDf) else spark.emptyDataFrame

    /*JOIN THE CURRENT AND PRIOR AGGREGATES TO POPULATE THE FINAL MEASURES METRICS*/
    val jndCurrRollingAggDf = currRollingAggDf.alias("curr").join(rollingAggDf.alias("pri"), Seq("parent_corp_key", "chain_id", "aggregation_rule_sk", "period_sk","sequential_product_level", "product_level"
      , "product_level_id", "product_level_desc", "product_level_parent_id", "brand_owner_filter_cd"))
      .select($"parent_corp_key", $"chain_id", $"aggregation_rule_sk", $"period_sk",$"sequential_product_level", $"product_level", $"product_level_id", $"product_level_desc",
        $"product_level_parent_id", $"brand_owner_filter_cd", col("curr.tot_sales").alias("curr_tot_sales"), $"pri.tot_sales".alias("pri_tot_sales"),
        $"curr.tot_units".alias("curr_tot_units"), $"pri.tot_units".alias("pri_tot_units"), $"curr.tot_trips".alias("curr_tot_trips"),
        $"pri.tot_trips".alias("pri_tot_trips"), $"curr.trip_penetration".alias("curr_trip_penetration"), $"pri.trip_penetration".alias("pri_trip_penetration"),
        $"curr.basket_size".alias("curr_basket_size"), $"pri.basket_size".alias("pri_basket_size")).withColumn("load_date", lit(s"${loadDte}"))

    /*JOIN THE CURRENT AND YAGO AGGREGATES TO POPULATE THE FINAL MEASURES METRICS*/
    val jndCurrYagoAggDf =  if(!dateRanges.periodTypQty.equals("52"))
    {currYagoAggDf.alias("curr").join(yagoAggDf.alias("yago"), Seq("parent_corp_key", "chain_id", "aggregation_rule_sk", "period_sk","sequential_product_level", "product_level"
      , "product_level_id", "product_level_desc", "product_level_parent_id", "brand_owner_filter_cd"))
      .select($"parent_corp_key", $"chain_id", $"aggregation_rule_sk", $"period_sk",$"sequential_product_level", $"product_level", $"product_level_id", $"product_level_desc",
        $"product_level_parent_id", $"brand_owner_filter_cd", col("curr.tot_sales").alias("curr_tot_sales"), $"yago.tot_sales".alias("yago_tot_sales"),
        $"curr.tot_units".alias("curr_tot_units"), $"yago.tot_units".alias("yago_tot_units"), $"curr.tot_trips".alias("curr_tot_trips"),
        $"yago.tot_trips".alias("yago_tot_trips"), $"curr.trip_penetration".alias("curr_trip_penetration"), $"yago.trip_penetration".alias("yago_trip_penetration"),
        $"curr.basket_size".alias("curr_basket_size"), $"yago.basket_size".alias("yago_basket_size")).withColumn("load_date", lit(s"${loadDte}")) }
    else
      spark.emptyDataFrame

    /*GENERATE FINAL AGGREGATED METRICS RECORDS*/
    jndCurrRollingAggDf.withColumn("prior_period_typ_cd", lit("R")).createOrReplaceTempView("currRollingtbl")

    val finalcurrRollingAggDf = spark.sql(
      """select chain_id,aggregation_rule_sk,period_sk,sequential_product_level,product_level,product_level_id,product_level_desc,
        |product_level_parent_id,brand_owner_filter_cd,prior_period_typ_cd,curr_tot_sales as curr_sales,pri_tot_sales as prev_sales,curr_tot_units as curr_units,
        |pri_tot_units as prev_units,curr_tot_trips as curr_trips,pri_tot_trips as prev_trips ,curr_trip_penetration as curr_trip_penetration,
        |pri_trip_penetration as prev_trip_penetration,curr_basket_size as curr_basket_size,pri_basket_size as prev_basket_size ,
        |parent_corp_key,load_date  from currRollingtbl""".stripMargin)

    if(!dateRanges.periodTypQty.equals("52"))
    {jndCurrYagoAggDf.withColumn("prior_period_typ_cd", lit("Y"))
      .createOrReplaceTempView("curryagotbl")}
    else
    {jndCurrRollingAggDf.withColumn("prior_period_typ_cd", lit("Y"))
      .createOrReplaceTempView("curryagotbl")}
    val finalcurrYagoAggDf = spark.sql(
      """select chain_id,aggregation_rule_sk,period_sk,sequential_product_level,product_level,product_level_id,product_level_desc,
        |product_level_parent_id,brand_owner_filter_cd,prior_period_typ_cd,curr_tot_sales as curr_sales,yago_tot_sales as prev_sales,curr_tot_units as curr_units,yago_tot_units as prev_units,
        |curr_tot_trips as curr_trips,yago_tot_trips as prev_trips,curr_trip_penetration as curr_trip_penetration,yago_trip_penetration as prev_trip_penetration,
        |curr_basket_size as curr_basket_size,yago_basket_size as prev_basket_size,parent_corp_key,load_date  from curryagotbl""".stripMargin)



    /*WRITE THE FINAL REPORTING AGGGREGATES TO THE TARGET TABLE*/
    finalcurrRollingAggDf.unionAll(finalcurrYagoAggDf)

  }

  /*FUNCTION TO FIND THE ACTIVE STORES */
  def getOrdWithActiveStores(spark: SparkSession, currDf: DataFrame, priorDf: DataFrame,dateRanges:DateRanges,periodStr:String,ownBrandDf:DataFrame) = {
    import spark.implicits._
    var ordDf = currDf.unionAll(priorDf).persist(StorageLevels.MEMORY_AND_DISK_SER)
    val currDateList =
      for {weekNo <- 0 until dateRanges.periodTypQty.toInt}
        yield {
          val dateRangeDf = spark.sql(s"select date_add('${dateRanges.currWkStrtDte}',${weekNo} * 7) as startDate,date_add(date_add('${dateRanges.currWkStrtDte}',6),${weekNo} * 7) as endDate")
          (dateRangeDf.select(col("startDate")).as[String].collect()(0), dateRangeDf.select(col("endDate")).as[String].collect()(0))
        }

    val priorWkStrtDte = if(periodStr.equals("rolling")) dateRanges.rollWkStrtDte else if (periodStr.equals("yago")) dateRanges.ygoWkStrtDte
    val priorWkEndDte =if(periodStr.equals("rolling")) dateRanges.rollWkEndDte else if (periodStr.equals("yago")) dateRanges.ygoWkEndDte
    val priorDateList =
      for {weekNo <- 0 until dateRanges.periodTypQty.toInt}
        yield {
          val dateRangeDf = spark.sql(s"select date_add('${priorWkStrtDte}',${weekNo} * 7) as startDate,date_add(date_add('$priorWkStrtDte',6),${weekNo} * 7) as endDate")
          (dateRangeDf.select(col("startDate")).as[String].collect()(0), dateRangeDf.select(col("endDate")).as[String].collect()(0))
        }

    val dateList = currDateList ++ priorDateList
    var df = ordDf
    for (x <- dateList) {
      val df1 = ordDf.filter(col("load_date") >= s"${x._1}" && col("load_date") <= s"${x._2}").select(col("store_id")).distinct
      df = df.as("ord").join(df1.as("actStr"), Seq("store_id")).select($"ord.*")
    }
    ordDf.unpersist()
    val currOrdWithActiveStoreDf = df.filter(col("load_date") >= s"${dateRanges.currWkStrtDte}" && col("load_date") <= s"${dateRanges.currWkEndDte}").withColumn("aggregation_rule_sk", lit(1))
      .unionAll(currDf.withColumn("aggregation_rule_sk", lit(0)))

    val priorOrdWithActiveStoreDf = df.filter(col("load_date") >= s"${priorWkStrtDte}" && col("load_date") <= s"${priorWkEndDte}").withColumn("aggregation_rule_sk", lit(1))
      .unionAll(priorDf.withColumn("aggregation_rule_sk", lit(0)))


    val currWithBrandCheckDf = checkOwnBrand(spark, currOrdWithActiveStoreDf,dateRanges.periodSk,ownBrandDf)
    val priorWithBrandCheckDf = checkOwnBrand(spark, priorOrdWithActiveStoreDf,dateRanges.periodSk,ownBrandDf)

    (currWithBrandCheckDf, priorWithBrandCheckDf)
  }


  def checkOwnBrand(spark: SparkSession, df: DataFrame,periodSk:String,ownBrandDf:DataFrame): DataFrame = {
    import spark.implicits._
    val ordDf = df.select($"id", $"chain_id", $"aggregation_rule_sk", $"store_id", $"parent_corp_key", $"date",
      $"trans_upc", $"amount", $"quantity", $"load_date")
    val jndDf = ordDf.as("ord").join(ownBrandDf.as("oBnd"), ordDf.col("trans_upc") === ownBrandDf.col("upc"), "left")
    val brandStatusDf = jndDf.withColumn("brand_owner_filter_cd", when(col("oBnd.upc").isNotNull, "O").otherwise("N"))
      .drop($"upc").withColumn("period_sk",lit(s"$periodSk"))
      .unionAll(ordDf.withColumn("brand_owner_filter_cd",lit("T")).withColumn("period_sk",lit(s"$periodSk")))
    brandStatusDf
  }


  def getProductHierarchy(spark: SparkSession, df: DataFrame, productHierarchyDf: DataFrame, productLevelLkpUpDf: DataFrame,
                          productHierachyLevel: String, parentProductHierarchyLevel: String) = {
    import spark.implicits._
    val seqPrdLevel = productLevelLkpUpDf.filter(col("description") === s"$productHierachyLevel").select(col("sequential_product_level")).as[Int].collect()(0)
    val productLevel = productLevelLkpUpDf.filter(col("description") === s"$productHierachyLevel").select(col("product_level")).as[Int].collect()(0)
    val arrayParentPrdLevel = productLevelLkpUpDf.filter(col("description") === s"$parentProductHierarchyLevel").select(col("product_level")).as[Int].collect()
    var parentProductLevel = ""
    if(!arrayParentPrdLevel.isEmpty)
      parentProductLevel = arrayParentPrdLevel(0).toString

    var parentProductLevelId = s"product_hierarchy_level_${parentProductLevel}_code"
    if (productHierarchyDf.columns.contains(s"$parentProductLevelId"))
      df.as("ord").join(productHierarchyDf.as("prdHrchy"), df.col("trans_upc") === productHierarchyDf.col("upc_code"))
        .select($"ord.*", $"prdHrchy.Product_hierarchy_Level_${productLevel}_Code".alias("product_level_id"),
          $"prdHrchy.Product_hierarchy_Level_${productLevel}_description".alias("product_level_desc"),$"prdHrchy.${parentProductLevelId}".alias("product_level_parent_id"))
        .withColumn("product_level", lit(productLevel)).withColumn("sequential_product_level", lit(seqPrdLevel))
    else
      df.as("ord").join(productHierarchyDf.as("prdHrchy"), df.col("trans_upc") === productHierarchyDf.col("upc_code"))
        .select($"ord.*", $"prdHrchy.Product_hierarchy_Level_${productLevel}_Code".alias("product_level_id"),
          $"prdHrchy.Product_hierarchy_Level_${productLevel}_description".alias("product_level_desc"),lit("").alias("product_level_parent_id"))
        .withColumn("product_level", lit(productLevel)).withColumn("sequential_product_level", lit(seqPrdLevel))

  }


  /*FUNCTION TO CREATE PRODUCT LEVEL AGGREGATES AND STORE LEVEL AGGREGATES */
  def createAgg(spark: SparkSession, df: DataFrame) = {
    import spark.implicits._
    /*Product Level Aggregates*/
    df.persist(StorageLevels.MEMORY_AND_DISK_SER)
    val prdLevelAgg = df.groupBy($"parent_corp_key", $"chain_id", $"aggregation_rule_sk", $"period_sk",$"sequential_product_level", $"product_level"
      , $"product_level_id", $"product_level_desc"
      , $"product_level_parent_id", $"brand_owner_filter_cd").agg(sum($"amount").alias("tot_sales"), sum($"quantity").alias("tot_units")
      , count($"id").alias("tot_trips"), (sum($"amount") / count($"id")).alias("basket_size"))

    /*Store Level Aggregates*/
    val strLevelAgg = df.groupBy($"parent_corp_key", $"chain_id", $"aggregation_rule_sk", $"period_sk", $"brand_owner_filter_cd").
      agg(count($"id").alias("tot_trips"))
    df.unpersist()
    /*Joining Product and Store level aggregates*/
    val jndAgg = prdLevelAgg.alias("prd").join(strLevelAgg.alias("str"), Seq("parent_corp_key", "chain_id", "aggregation_rule_sk", "period_sk", "brand_owner_filter_cd"))
      .withColumn("trip_penetration", ($"prd.tot_trips" / $"str.tot_trips")).drop($"str.tot_trips")
    jndAgg
  }

}


