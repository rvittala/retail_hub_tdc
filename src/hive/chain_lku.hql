DROP TABLE IF EXISTS ${hiveconf:database_name}.chain_lku;

CREATE EXTERNAL TABLE ${hiveconf:database_name}.chain_lku(
    parent_corp_key int,
    chain_id int,
    chain_id_desc string,
    active_ind int,
    create_dtm date,
    last_updt_dtm date
)              
STORED AS ORC 
LOCATION 'adl://dlpreastus2adls1.azuredatalakestore.net/catmktg-edl-prod/data/refined/edw4x/${hiveconf:database_name}.db/chain_lku';