use ${hiveconf:database_name};

insert into aggregation_rule_lku values(0,'All Stores','Aggregate all stores',1,current_date(),current_date()),(1,'Comp Store','Aggregate only stores that are active in the current period and the prior period.  Active=\'Has at least 1 transaction in every single week of that time period, regardless of product dimension\'',1,current_date(),current_date());

insert into brand_owner_filter_lku values('T','Total','Anything in the store, no filters to any list',1,current_date(),current_date()),('O','Own Brand','Product on the own brand list',1,current_date(),current_date()),('N','Not Own Brand','Anything in the store not on the own brand list',1,current_date(),current_date());

insert into prior_period_lku values('R','Rolling','The prior period is the period right before the given time period.',1,current_date(),current_date()),('Y','Year Ago','The prior period is the period 52 weeks ago',1,current_date(),current_date());

insert into product_level_lku values(4,1,5,'Department',1,current_date(),current_date()),(4,2,3,'Category',1,current_date(),current_date()),(4,3,2,'Subcategory',1,current_date(),current_date());