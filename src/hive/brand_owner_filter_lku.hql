DROP TABLE IF EXISTS ${hiveconf:database_name}.brand_owner_filter_lku;

CREATE EXTERNAL TABLE ${hiveconf:database_name}.brand_owner_filter_lku(
    brand_owner_filter_cd string,
    brand_owner_filter_nm string,
    brand_owner_filter_desc string,
    active_ind int,
    create_dtm date,
    last_updt_dtm date
)              
STORED AS ORC 
LOCATION 'adl://dlpreastus2adls1.azuredatalakestore.net/catmktg-edl-prod/data/refined/edw4x/${hiveconf:database_name}.db/brand_owner_filter_lku';