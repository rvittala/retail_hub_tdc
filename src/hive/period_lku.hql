DROP TABLE IF EXISTS ${hiveconf:database_name}.period_lku;

CREATE EXTERNAL TABLE ${hiveconf:database_name}.period_lku(
    period_sk int,
    period_nm string,
    period_typ_nm string,
    period_typ_cd string,
    period_typ_qty int,
    period_begin_dt date,
    period_end_dt date,
    active_ind int,
    create_dtm date,
    last_updt_dtm date
)              
STORED AS ORC 
LOCATION 'adl://dlpreastus2adls1.azuredatalakestore.net/catmktg-edl-prod/data/refined/edw4x/${hiveconf:database_name}.db/period_lku';