DROP TABLE IF EXISTS ${hiveconf:database_name}.aggregation_rule_lku;

CREATE EXTERNAL TABLE ${hiveconf:database_name}.aggregation_rule_lku(
    aggregation_rule_sk int,
    aggregation_rule_nm string,
    aggregation_rule_desc string,
    active_ind int,
    create_dtm date,
    last_updt_dtm date
)              
STORED AS ORC 
LOCATION 'adl://dlpreastus2adls1.azuredatalakestore.net/catmktg-edl-prod/data/refined/edw4x/${hiveconf:database_name}.db/aggregation_rule_lku';