DROP TABLE IF EXISTS ${hiveconf:database_name}.product_level_lku;

CREATE EXTERNAL TABLE ${hiveconf:database_name}.product_level_lku(
    parent_corp_key int,
    sequential_product_level int,
    product_level int,
    description string,
    active_ind int,
    create_dtm date,
    last_updt_dtm date
)              
STORED AS ORC 
LOCATION 'adl://dlpreastus2adls1.azuredatalakestore.net/catmktg-edl-prod/data/refined/edw4x/${hiveconf:database_name}.db/product_level_lku';