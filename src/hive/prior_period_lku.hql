DROP TABLE IF EXISTS ${hiveconf:database_name}.prior_period_lku;

CREATE EXTERNAL TABLE ${hiveconf:database_name}.prior_period_lku(
    prior_period_typ_cd string,
    prior_period_typ_nm string,
    prior_period_typ_desc string,
    active_ind int,
    create_dtm date,
    last_updt_dtm date
)              
STORED AS ORC 
LOCATION 'adl://dlpreastus2adls1.azuredatalakestore.net/catmktg-edl-prod/data/refined/edw4x/${hiveconf:database_name}.db/prior_period_lku';