USE ${hiveconf:database_name};

insert into period_lku select row_number() over(order by period_end_dt,period_begin_dt desc)as period_sk,* from 
(
select 
   case when cal_dow_nbr=1 then concat("1 Week ending ",date_sub(wk_ending_dt,0))
      when cal_dow_nbr=2 then concat("1 Week ending ",date_sub(wk_ending_dt,6))
      when cal_dow_nbr=3 then concat("1 Week ending ",date_sub(wk_ending_dt,5))
      when cal_dow_nbr=4 then concat("1 Week ending ",date_sub(wk_ending_dt,4))
      when cal_dow_nbr=5 then concat("1 Week ending ",date_sub(wk_ending_dt,3))
      when cal_dow_nbr=6 then concat("1 Week ending ",date_sub(wk_ending_dt,2))
  else concat("1 Week ending ",date_sub(wk_ending_dt,1)) end as period_nm,
  "week" as period_typ_nm,"week" as period_typ_cd,1 as period_typ_qty,
  case when cal_dow_nbr=1 then date_sub(wk_beginning_dt,0)
      when cal_dow_nbr=2 then date_sub(wk_beginning_dt,6)
      when cal_dow_nbr=3 then date_sub(wk_beginning_dt,5)
      when cal_dow_nbr=4 then date_sub(wk_beginning_dt,4)
      when cal_dow_nbr=5 then date_sub(wk_beginning_dt,3)
      when cal_dow_nbr=6 then date_sub(wk_beginning_dt,2)
  else date_sub(wk_beginning_dt,1) end as period_begin_dt,
  case when cal_dow_nbr=1 then date_sub(wk_ending_dt,0)
      when cal_dow_nbr=2 then date_sub(wk_ending_dt,6)
      when cal_dow_nbr=3 then date_sub(wk_ending_dt,5)
      when cal_dow_nbr=4 then date_sub(wk_ending_dt,4)
      when cal_dow_nbr=5 then date_sub(wk_ending_dt,3)
      when cal_dow_nbr=6 then date_sub(wk_ending_dt,2)
  else date_sub(wk_ending_dt,1) end as period_end_dt,
  1 as active_ind,current_date() as create_dtm,current_date() as last_updt_dtm 
  from ref_edw4x_pn1useh1.date_t where cal_dow_nbr in (7) and cal_yr_nbr in (2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024)
union all
select 
case when cal_dow_nbr=1 then concat("4 Week ending ",date_sub(wk_ending_dt,0))
      when cal_dow_nbr=2 then concat("4 Week ending ",date_sub(wk_ending_dt,6))
      when cal_dow_nbr=3 then concat("4 Week ending ",date_sub(wk_ending_dt,5))
      when cal_dow_nbr=4 then concat("4 Week ending ",date_sub(wk_ending_dt,4))
      when cal_dow_nbr=5 then concat("4 Week ending ",date_sub(wk_ending_dt,3))
      when cal_dow_nbr=6 then concat("4 Week ending ",date_sub(wk_ending_dt,2))
  else concat("4 Weeks ending ",date_sub(wk_ending_dt,1)) end as period_nm,
  "weeks" as period_typ_nm,"week" as period_typ_cd,4 as period_typ_qty,
  case when cal_dow_nbr=1 then date_sub(wk_beginning_dt,21)
      when cal_dow_nbr=2 then date_sub(wk_beginning_dt,27)
      when cal_dow_nbr=3 then date_sub(wk_beginning_dt,26)
      when cal_dow_nbr=4 then date_sub(wk_beginning_dt,25)
      when cal_dow_nbr=5 then date_sub(wk_beginning_dt,24)
      when cal_dow_nbr=6 then date_sub(wk_beginning_dt,23)
  else date_sub(wk_beginning_dt,22) end as period_begin_dt,
   case when cal_dow_nbr=1 then date_sub(wk_ending_dt,0)
      when cal_dow_nbr=2 then date_sub(wk_ending_dt,6)
      when cal_dow_nbr=3 then date_sub(wk_ending_dt,5)
      when cal_dow_nbr=4 then date_sub(wk_ending_dt,4)
      when cal_dow_nbr=5 then date_sub(wk_ending_dt,3)
      when cal_dow_nbr=6 then date_sub(wk_ending_dt,2)
  else date_sub(wk_ending_dt,1) end as period_end_dt,
  1 as active_ind,current_date() as create_dtm,current_date() as last_updt_dtm 
  from ref_edw4x_pn1useh1.date_t where cal_dow_nbr in (7) and cal_yr_nbr in (2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024)
union all
select 
case when cal_dow_nbr=1 then concat("12 Week ending ",date_sub(wk_ending_dt,0))
      when cal_dow_nbr=2 then concat("12 Week ending ",date_sub(wk_ending_dt,6))
      when cal_dow_nbr=3 then concat("12 Week ending ",date_sub(wk_ending_dt,5))
      when cal_dow_nbr=4 then concat("12 Week ending ",date_sub(wk_ending_dt,4))
      when cal_dow_nbr=5 then concat("12 Week ending ",date_sub(wk_ending_dt,3))
      when cal_dow_nbr=6 then concat("12 Week ending ",date_sub(wk_ending_dt,2))
  else concat("12 Weeks ending ",date_sub(wk_ending_dt,1)) end as period_nm,
  "weeks" as period_typ_nm,"week" as period_typ_cd,12 as period_typ_qty,
  case when cal_dow_nbr=1 then date_sub(wk_beginning_dt,77)
      when cal_dow_nbr=2 then date_sub(wk_beginning_dt,83)
      when cal_dow_nbr=3 then date_sub(wk_beginning_dt,82)
      when cal_dow_nbr=4 then date_sub(wk_beginning_dt,81)
      when cal_dow_nbr=5 then date_sub(wk_beginning_dt,80)
      when cal_dow_nbr=6 then date_sub(wk_beginning_dt,79)
  else date_sub(wk_beginning_dt,78) end as period_begin_dt,
  case when cal_dow_nbr=1 then date_sub(wk_ending_dt,0)
      when cal_dow_nbr=2 then date_sub(wk_ending_dt,6)
      when cal_dow_nbr=3 then date_sub(wk_ending_dt,5)
      when cal_dow_nbr=4 then date_sub(wk_ending_dt,4)
      when cal_dow_nbr=5 then date_sub(wk_ending_dt,3)
      when cal_dow_nbr=6 then date_sub(wk_ending_dt,2)
  else date_sub(wk_ending_dt,1) end as period_end_dt,
  1 as active_ind,current_date() as create_dtm,current_date() as last_updt_dtm from ref_edw4x_pn1useh1.date_t where cal_dow_nbr in (7) and cal_yr_nbr in (2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024)
union all
select 
case when cal_dow_nbr=1 then concat("26 Week ending ",date_sub(wk_ending_dt,0))
      when cal_dow_nbr=2 then concat("26 Week ending ",date_sub(wk_ending_dt,6))
      when cal_dow_nbr=3 then concat("26 Week ending ",date_sub(wk_ending_dt,5))
      when cal_dow_nbr=4 then concat("26 Week ending ",date_sub(wk_ending_dt,4))
      when cal_dow_nbr=5 then concat("26 Week ending ",date_sub(wk_ending_dt,3))
      when cal_dow_nbr=6 then concat("26 Week ending ",date_sub(wk_ending_dt,2))
  else concat("26 Weeks ending ",date_sub(wk_ending_dt,1)) end as period_nm,
  "weeks" as period_typ_nm,"week" as period_typ_cd,26 as period_typ_qty,
  case when cal_dow_nbr=1 then date_sub(wk_beginning_dt,175)
      when cal_dow_nbr=2 then date_sub(wk_beginning_dt,181)
      when cal_dow_nbr=3 then date_sub(wk_beginning_dt,180)
      when cal_dow_nbr=4 then date_sub(wk_beginning_dt,179)
      when cal_dow_nbr=5 then date_sub(wk_beginning_dt,178)
      when cal_dow_nbr=6 then date_sub(wk_beginning_dt,177)
  else date_sub(wk_beginning_dt,176) end as period_begin_dt,
  case when cal_dow_nbr=1 then date_sub(wk_ending_dt,0)
      when cal_dow_nbr=2 then date_sub(wk_ending_dt,6)
      when cal_dow_nbr=3 then date_sub(wk_ending_dt,5)
      when cal_dow_nbr=4 then date_sub(wk_ending_dt,4)
      when cal_dow_nbr=5 then date_sub(wk_ending_dt,3)
      when cal_dow_nbr=6 then date_sub(wk_ending_dt,2)
  else date_sub(wk_ending_dt,1) end as period_end_dt,1 as active_ind,current_date() as create_dtm,current_date() as last_updt_dtm from ref_edw4x_pn1useh1.date_t where cal_dow_nbr in (7) and cal_yr_nbr in (2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024)
union all
select 
 case when cal_dow_nbr=1 then concat("52 Week ending ",date_sub(wk_ending_dt,0))
      when cal_dow_nbr=2 then concat("52 Week ending ",date_sub(wk_ending_dt,6))
      when cal_dow_nbr=3 then concat("52 Week ending ",date_sub(wk_ending_dt,5))
      when cal_dow_nbr=4 then concat("52 Week ending ",date_sub(wk_ending_dt,4))
      when cal_dow_nbr=5 then concat("52 Week ending ",date_sub(wk_ending_dt,3))
      when cal_dow_nbr=6 then concat("52 Week ending ",date_sub(wk_ending_dt,2))
  else concat("52 Weeks ending ",date_sub(wk_ending_dt,1)) end as period_nm,
  "weeks" as period_typ_nm,"week" as period_typ_cd,52 as period_typ_qty,
  case when cal_dow_nbr=1 then date_sub(wk_beginning_dt,357)
      when cal_dow_nbr=2 then date_sub(wk_beginning_dt,363)
      when cal_dow_nbr=3 then date_sub(wk_beginning_dt,362)
      when cal_dow_nbr=4 then date_sub(wk_beginning_dt,361)
      when cal_dow_nbr=5 then date_sub(wk_beginning_dt,360)
      when cal_dow_nbr=6 then date_sub(wk_beginning_dt,359)
  else date_sub(wk_beginning_dt,358) end as period_begin_dt,
  case when cal_dow_nbr=1 then date_sub(wk_ending_dt,0)
      when cal_dow_nbr=2 then date_sub(wk_ending_dt,6)
      when cal_dow_nbr=3 then date_sub(wk_ending_dt,5)
      when cal_dow_nbr=4 then date_sub(wk_ending_dt,4)
      when cal_dow_nbr=5 then date_sub(wk_ending_dt,3)
      when cal_dow_nbr=6 then date_sub(wk_ending_dt,2)
  else date_sub(wk_ending_dt,1) end as period_end_dt,1 as active_ind,
  current_date() as create_dtm,current_date() as last_updt_dtm from ref_edw4x_pn1useh1.date_t where cal_dow_nbr in (7) and cal_yr_nbr in (2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024)) res;