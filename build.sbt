
name := "Retail_Hub_2.0"

version := "0.1"

scalaVersion := "2.11.8"

scalaVersion := "2.11.8"

val sparkVersion = "2.2.0"

resolvers += Resolver.jcenterRepo

libraryDependencies ++= Seq(
  "com.beust" % "jcommander" % "1.72",
  "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-hive" % sparkVersion % "provided",
  "com.lmax" % "disruptor" % "3.3.6",
  "net.liftweb" %% "lift-json" % "3.0",
  "joda-time" % "joda-time" % "2.8.1",
  "nl.basjes.parse.useragent" % "yauaa" % "5.7",
  "org.apache.commons" % "commons-collections4" % "4.2"
)

assemblyJarName in assembly := s"${name.value}-${version.value}.jar"